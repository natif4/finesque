#ifndef DEVICEMANAGER_H
#define DEVICEMANAGER_H

#include <QString>

class DeviceManager
{
public:
    DeviceManager(const QString &a_device,
                  const QString &a_version);

    QString get_device_id();
    QByteArray get_device_information();
    QByteArray get_device_information_with_token(const QString &a_token);

private:
    const size_t DEVICE_ID_LENGHT = 10;
    const QString CHAR_SET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    QString _device;
    QString _device_id;
    QString _version;

    QString get_device_id_from_file();
    QString generate_device_id();
};

#endif // DEVICEMANAGER_H
