#include "logininformation.h"

LoginInformation::LoginInformation(const QString &a_username, const QString &a_password)
{
    _username = a_username;
    _password = a_password;
}

QString LoginInformation::get_username()
{
    return _username;
}

QString LoginInformation::get_password()
{
    return _password;
}

void LoginInformation::set_username(const QString &a_username)
{
    _username = a_username;
}

void LoginInformation::set_password(const QString &a_password)
{
    _username = a_password;
}
