#ifndef AUTHENTICATIONWINDOW_H
#define AUTHENTICATIONWINDOW_H

#include <QMainWindow>
#include "userservice.h"

namespace Ui {
class AuthenticationWindow;
}

class AuthenticationWindow : public QWidget
{
    Q_OBJECT

public:
    AuthenticationWindow(QWidget *a_parent = nullptr);
    ~AuthenticationWindow();

private slots:
    void on_loginButton_clicked();

private:
    const int PROGRESS_BAR_MAX_VALUE = 100;
    const int PROGRESS_BAR_MIN_VALUE = 0;

    Ui::AuthenticationWindow *_ui;
    UserService *_user_service;

    void initialize_progress_bar();
    void on_authentication_finished(QNetworkReply *a_reply);
};
#endif // AUTHENTICATIONWINDOW_H
