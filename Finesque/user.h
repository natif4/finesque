#ifndef USER_H
#define USER_H

#include <QString>

class User
{
public:
    User(const QString &a_name,
         const QString &a_id,
         const QString &a_accessToken);

    QString get_name();
    QString get_id();
    QString get_access_token();

private:
    QString _name;
    QString _id;
    QString _access_token;
};

#endif // USER_H
