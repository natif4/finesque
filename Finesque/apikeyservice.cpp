#include "apikeyservice.h"

ApiKeyService::ApiKeyService() : ApiService()
{

}

ApiKeyService::ApiKeyService(const QString &a_token) : ApiService(a_token)
{

}

ApiKeyService::~ApiKeyService()
{
    this->ApiService::~ApiService();
}

QNetworkReply* ApiKeyService::get_api_keys()
{
    set_url(ENDPOINT);
    return _network_manager->get(_request);
}

QNetworkReply* ApiKeyService::create_api_key()
{
    set_url(ENDPOINT);
    QUrlQuery query;
    query.addQueryItem("app", API_KEY_NAME);
    set_url_query(query);
    return _network_manager->post(_request, "");
}

QNetworkReply* ApiKeyService::revoke_api_key(const QString &a_token)
{
    set_url(ENDPOINT + "/" + a_token);
    return _network_manager->deleteResource(_request);
}

QString ApiKeyService::get_api_key_name()
{
    return API_KEY_NAME;
}
