#include "devicemanager.h"
#include "QDebug"
#include <QFile>
#include <QDir>

DeviceManager::DeviceManager(const QString &a_device,
                             const QString &a_version)
{
    _device = a_device;
    _version = a_version;
    _device_id = get_device_id_from_file();
}

QString DeviceManager::get_device_id()
{
    return _device_id;
}

QString DeviceManager::get_device_id_from_file()
{
    QString device_id = generate_device_id();
    QFile file(QDir::currentPath() + "/device_id.txt");
    QTextStream stream(&file);
    if (file.open(QIODevice::ReadWrite))
    {
        QString line = stream.readAll();
        if (line != "")
        {
            device_id = line;
        }
        else
        {
            stream << device_id;
        }
    }
    file.close();
    return device_id;
}

QString DeviceManager::generate_device_id()
{
    QString device_id = "";
    device_id.reserve(DEVICE_ID_LENGHT);
    for (size_t i = 0; i < DEVICE_ID_LENGHT; i++)
    {
        device_id.append(CHAR_SET[rand() % CHAR_SET.size()]);
    }
    return device_id;
}

QByteArray DeviceManager::get_device_information()
{
    QString device_information = "MediaBrowser Client=\"" + _device +
                                "\", Device=\"" + _device +
                                "\", DeviceId=\"" + _device_id +
                                "\", Version=\"" + _version +
                                "\"";
    return device_information.toUtf8();
}

QByteArray DeviceManager::get_device_information_with_token(const QString &a_token)
{
    QString device_information = get_device_information() + ", Token=\"" + a_token + "\"";
    return device_information.toUtf8();
}
