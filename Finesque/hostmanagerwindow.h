#ifndef HOSTMANAGERWINDOW_H
#define HOSTMANAGERWINDOW_H

#include <QWidget>
#include "hostmanager.h"

namespace Ui {
class HostManagerWindow;
}

class HostManagerWindow : public QWidget
{
    Q_OBJECT

public:
    explicit HostManagerWindow(QWidget *a_parent = nullptr);
    ~HostManagerWindow();

    bool ready_to_hide();

private slots:
    void on_configureButton_clicked();

private:
    Ui::HostManagerWindow *_ui;

    HostManager *_host_manager;
    bool _ready_to_hide;

    void initialize_loading_window();
    void initialize_host_window();
    void check_host_url();
    void go_to_authentication();
};

#endif // HOSTMANAGERWINDOW_H
