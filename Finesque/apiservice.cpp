#include "apiservice.h"

ApiService::ApiService()
{
    _network_manager = new QNetworkAccessManager();
    _device_manager = new DeviceManager(QString("Finesque"), QString("0.1"));
    _host_manager = new HostManager();
    _host_url = _host_manager->get_host_url();
    initialize_request();
}

ApiService::ApiService(const QString &a_token)
{
    _network_manager = new QNetworkAccessManager();
    _device_manager = new DeviceManager(QString("Finesque"), QString("0.1"));
    _host_manager = new HostManager();
    _host_url = _host_manager->get_host_url();
    _request.setUrl(QUrl(_host_url));
    set_content_type(ContentType::JSON);
    set_request_token(a_token);
}

ApiService::~ApiService()
{
    delete _network_manager;
    delete _device_manager;
    delete _host_manager;
}

void ApiService::set_url(const QString &a_parameter)
{
    _request.setUrl(_host_url + a_parameter);
}

void ApiService::set_url_query(QUrlQuery a_query)
{
    QUrl url(_request.url().url());
    url.setQuery(a_query.query());
    _request.setUrl(url);
}

void ApiService::set_request_token(const QString &a_token)
{
    _request.setRawHeader(AUTHORIZATION_HEADER, _device_manager->get_device_information_with_token(a_token));
}

void ApiService::set_content_type(ContentType a_type)
{
    _request.setHeader(QNetworkRequest::ContentTypeHeader, CONTENT_TYPES.at(a_type));
}

void ApiService::initialize_request()
{
    _request.setUrl(QUrl(_host_url));
    set_content_type(ContentType::JSON);
    _request.setRawHeader(AUTHORIZATION_HEADER, _device_manager->get_device_information());
}






