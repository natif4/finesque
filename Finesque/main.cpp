#include "hostmanagerwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication application(argc, argv);
    HostManagerWindow window;
    window.show();
    if (window.ready_to_hide())
    {
        window.hide();
    }
    return application.exec();
}
