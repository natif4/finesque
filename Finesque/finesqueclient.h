#ifndef FINESQUECLIENT_H
#define FINESQUECLIENT_H

#include <QMainWindow>
#include "user.h"
#include "userservice.h"
#include "audioservice.h"
#include "apikeyservice.h"
#include "itemservice.h"
#include <QListWidgetItem>
#include <QMediaPlayer>
#include <QAudioOutput>
#include "song.h"

QT_BEGIN_NAMESPACE
namespace Ui { class FinesqueClient; }
QT_END_NAMESPACE

class FinesqueClient : public QMainWindow
{
    Q_OBJECT

public:
    explicit FinesqueClient(User *a_user, QWidget *a_parent = nullptr);
    ~FinesqueClient();

private slots:
    void on_list_item_clicked(QListWidgetItem *a_item);
    void on_logoutButton_clicked();
    void on_playButton_clicked();
    void on_stopButton_clicked();
    void on_volumeSlider_valueChanged(int value);
    void on_durationSlider_valueChanged(int value);
    void on_loopButton_clicked(bool checked);
    void on_nextButton_clicked();
    void on_previousButton_clicked();
    void on_randomButton_clicked(bool checked);

private:
    const int SONG_LIST_INDEX = 0;
    const int MEDIA_PLAYER_INDEX = 1;
    const int BASE_VOLUME = 50;
    const float MAX_VOLUME = 100;

    Ui::FinesqueClient *_ui;
    User *_user;
    Song *_current_song;
    UserService *_user_service;
    AudioService *_audio_service;
    ApiKeyService *_api_key_service;
    ItemService *_item_service;
    QList<Song*> *_songs;
    QMediaPlayer *_player;
    QAudioOutput *_audio_output;
    bool _is_random;
    QList<Song*> *_random_songs;
    QString _api_key;

    void initialize_client_information();
    void initialize_player_information();
    void on_song_download_finished(QNetworkReply *a_reply);
    void on_song_image_download_finished(QNetworkReply *a_reply, Song *a_song);
    void on_song_audio_download_finished(QNetworkReply *a_reply, Song *a_song);
    void on_api_keys_obtained(QNetworkReply *a_reply);
    bool find_api_key(QNetworkReply *a_reply);
    void create_api_key();
    void load_song(Song *a_song);
    void connect_events_to_player();
    void go_to_next_song();
};

#endif // FINESQUECLIENT_H
