#ifndef JSONSERIALIZER_H
#define JSONSERIALIZER_H

#include <QByteArray>
#include <QJsonObject>
#include <QNetworkReply>

class JsonSerializer
{
public:
    static QByteArray serialize(QJsonObject a_body);
    static QJsonObject deserialize(QNetworkReply *a_reply);
};

#endif // JSONSERIALIZER_H
