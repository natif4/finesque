#include "jsonserializer.h"
#include <QJsonDocument>

QByteArray JsonSerializer::serialize(QJsonObject a_body)
{
    QJsonDocument document(a_body);
    return document.toJson();
}

QJsonObject JsonSerializer::deserialize(QNetworkReply *a_reply)
{
    QByteArray data = a_reply->readAll();
    QJsonDocument doc(QJsonDocument::fromJson(data));
    return doc.object();
}


