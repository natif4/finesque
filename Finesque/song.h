#ifndef SONG_H
#define SONG_H

#include <QString>
#include <QPixmap>

class Song
{
public:
    Song(const QString &a_id,
         const QString &a_title,
         const QString &a_artist,
         const QString &a_album,
         const QString &a_image_tag,
         const QImage a_icon = QImage());

    QString get_id();
    QString get_title();
    QString get_artist();
    QString get_album();
    QString get_image_tag();
    QImage get_icon();
    void set_icon(QImage a_icon);

private:
    QString _id;
    QString _title;
    QString _artist;
    QString _album;
    QString _image_tag;
    QImage _icon;
};

#endif // SONG_H
