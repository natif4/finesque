#ifndef ITEMSERVICE_H
#define ITEMSERVICE_H

#include "apiservice.h"
#include "song.h"

class ItemService : public ApiService
{
public:
    ItemService();
    ItemService(const QString &a_token);
    ~ItemService();

    QNetworkReply* get_song_image(Song *a_song);

private:
    const QString ENDPOINT = "Items";
};

#endif // ITEMSERVICE_H
