#include "authenticationwindow.h"
#include "ui_authenticationwindow.h"
#include "userservice.h"
#include "logininformation.h"
#include "finesqueclient.h"
#include "jsonserializer.h"

AuthenticationWindow::AuthenticationWindow(QWidget *a_parent): QWidget(a_parent), _ui(new Ui::AuthenticationWindow)
{
    _ui->setupUi(this);
    _user_service = new UserService;
    initialize_progress_bar();
}

AuthenticationWindow::~AuthenticationWindow()
{
    delete _ui;
    delete _user_service;
}

void AuthenticationWindow::on_loginButton_clicked()
{
    _ui->loginButton->hide();
    _ui->progressBar->show();
    LoginInformation login_information(_ui->usernameEdit->text(), _ui->passwordEdit->text());
    QNetworkReply *reply = _user_service->authenticate_by_name(login_information);

    _ui->loginLabel->setStyleSheet("QLabel { color : black; }"); // TODO - Put in ColorManager class (or other "UI" manager)
    _ui->loginLabel->setText("Pending...");

    QObject::connect(reply, &QNetworkReply::finished, [=]()
    {
        on_authentication_finished(reply);
    });
}

void AuthenticationWindow::on_authentication_finished(QNetworkReply *a_reply)
{
    if(a_reply->error() == QNetworkReply::NoError)
    {
        for (int i = 0; i < 100; i++)
        {
            _ui->progressBar->setValue(i);
        }
        QJsonObject json = JsonSerializer::deserialize(a_reply);
        QJsonObject json_user = json["User"].toObject();

        User *user = new User(json_user["Name"].toString(),
                              json_user["Id"].toString(),
                              json["AccessToken"].toString());

        _ui->loginLabel->setText("Done!");
        FinesqueClient *client = new FinesqueClient(user);
        client->show();
        destroy();
    }
    else
    {
        qDebug() << a_reply->errorString();
        _ui->loginLabel->setStyleSheet("QLabel { color : red; }"); // TODO - Put in ColorManager class (or other "UI" manager)
        _ui->loginLabel->setText("Authentication failed.");
        _ui->progressBar->hide();
        _ui->loginButton->show();
    }
    a_reply->deleteLater();
}

void AuthenticationWindow::initialize_progress_bar()
{
    _ui->progressBar->setValue(PROGRESS_BAR_MIN_VALUE);
    _ui->progressBar->setMinimum(PROGRESS_BAR_MIN_VALUE);
    _ui->progressBar->setMaximum(PROGRESS_BAR_MAX_VALUE);
    _ui->progressBar->hide();
}

