#ifndef LOGININFORMATION_H
#define LOGININFORMATION_H

#include <QString>

class LoginInformation
{
public:
    LoginInformation(const QString &a_username, const QString &a_password);
    QString get_username();
    QString get_password();
    void set_username(const QString &a_username);
    void set_password(const QString &a_password);

private:
    QString _username;
    QString _password;
};

#endif // LOGININFORMATION_H
