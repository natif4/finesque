#ifndef AUDIOSERVICE_H
#define AUDIOSERVICE_H

#include "apiservice.h"
#include <QString>

class AudioService : public ApiService
{
public:
    AudioService();
    AudioService(const QString &a_token);
    ~AudioService();

    QString get_audio(const QString &a_audio_id,
                             const QString &a_user_id,
                             const QString &a_api_key);

private:
    const QString ENDPOINT = "Audio";

};

#endif // AUDIOSERVICE_H
