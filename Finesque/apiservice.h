#ifndef APISERVICE_H
#define APISERVICE_H

#include <string>
#include <QtNetwork/QNetworkAccessManager>
#include <QList>
#include <QJsonObject>
#include <QJsonDocument>
#include <QNetworkReply>
#include "devicemanager.h"
#include "hostmanager.h"
#include <QUrlQuery>

class ApiService
{
public:
    ApiService();
    ApiService(const QString &a_token);
    virtual ~ApiService() = 0;

protected:
    QNetworkAccessManager *_network_manager;
    QNetworkRequest _request;
    DeviceManager *_device_manager;
    HostManager *_host_manager;

    enum ContentType { JSON, MP3 };

    void set_url(const QString &a_parameter);
    void set_url_query(QUrlQuery a_query);
    void set_content_type(ContentType a_type);
    void set_request_token(const QString &a_token);

private:
    QString _host_url;

    const QByteArray AUTHORIZATION_HEADER = "X-Emby-Authorization";
    const QStringList CONTENT_TYPES =
            QStringList(
                {
                    QString("application/json"),
                    QString("audio/mp3")
                });

    void initialize_request();
};
#endif // APISERVICE_H
