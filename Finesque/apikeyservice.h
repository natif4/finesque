#ifndef APIKEYSERVICE_H
#define APIKEYSERVICE_H

#include "apiservice.h"
#include <QString>

class ApiKeyService : public ApiService
{
public:
    ApiKeyService();
    ApiKeyService(const QString &a_token);
    ~ApiKeyService();

    QNetworkReply* get_api_keys();
    QNetworkReply* create_api_key();
    QNetworkReply* revoke_api_key(const QString &a_token);
    QString get_api_key_name();

private:
    const QString ENDPOINT = "Auth/Keys";
    const QString API_KEY_NAME = "Finesque";
};

#endif // APIKEYSERVICE_H
