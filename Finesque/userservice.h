#ifndef USERSERVICE_H
#define USERSERVICE_H

#include "user.h"
#include "apiservice.h"
#include "logininformation.h"

class UserService: public ApiService
{
public:
    UserService();
    UserService(const QString &a_token, const QString &a_user_id);
    ~UserService();

    QNetworkReply* authenticate_by_name(LoginInformation a_login_information);
    QNetworkReply* get_songs();

private:
    const QString ENDPOINT = "Users";

    QString _user_id;
};

#endif // USERSERVICE_H
