#include "hostmanagerwindow.h"
#include "ui_hostmanagerwindow.h"
#include "authenticationwindow.h"

HostManagerWindow::HostManagerWindow(QWidget *a_parent) : QWidget(a_parent), _ui(new Ui::HostManagerWindow)
{
    _ui->setupUi(this);
    _ready_to_hide = false;
    _host_manager = new HostManager;
    initialize_loading_window();
    check_host_url();
}

HostManagerWindow::~HostManagerWindow()
{
    delete _ui;
    delete _host_manager;
}

bool HostManagerWindow::ready_to_hide()
{
    return _ready_to_hide;
}

void HostManagerWindow::initialize_loading_window()
{
    _ui->configureButton->hide();
    _ui->schemeLabel->hide();
    _ui->schemeEdit->hide();
    _ui->hostnameLabel->hide();
    _ui->hostnameEdit->hide();
    _ui->portLabel->hide();
    _ui->portEdit->hide();
    _ui->configureLabel->setText("Application starting...");
    for (int i = 0; i < 100; i++)
    {
        _ui->progressBar->setValue(i);
    }
}

void HostManagerWindow::initialize_host_window()
{
    _ui->configureLabel->setText("Configure Jellyfin Host");
    _ui->progressBar->hide();
    _ui->configureButton->show();
    _ui->schemeLabel->show();
    _ui->schemeEdit->show();
    _ui->hostnameLabel->show();
    _ui->hostnameEdit->show();
    _ui->portLabel->show();
    _ui->portEdit->show();
}

void HostManagerWindow::check_host_url()
{
    QString url = _host_manager->get_host_url();
    if (url == "")
    {
        initialize_host_window();
    }
    else
    {
        go_to_authentication();
    }
}

void HostManagerWindow::go_to_authentication()
{
    AuthenticationWindow *auth = new AuthenticationWindow;
    auth->show();
    this->hide();
    _ready_to_hide = true;
}

void HostManagerWindow::on_configureButton_clicked()
{
    if (_ui->schemeEdit->text() != "" &&
        _ui->hostnameEdit->text() != "" &&
        _ui->portEdit->text() != "")
    {
        _host_manager->create_host_url(_ui->schemeEdit->text(),
                                      _ui->hostnameEdit->text(),
                                      _ui->portEdit->text());
        go_to_authentication();
    }
    else
    {
        _ui->configureLabel->setStyleSheet("QLabel { color : red; }");
        _ui->configureLabel->setText("Configuration parameters missing.");
    }
}

