#include "song.h"

Song::Song(const QString &a_id,
           const QString &a_title,
           const QString &a_artist,
           const QString &a_album,
           const QString &a_image_tag,
           const QImage a_icon)
{
    _id = a_id;
    _title = a_title;
    _artist = a_artist;
    _album = a_album;
    _image_tag = a_image_tag;
    _icon = a_icon;
}

QString Song::get_id()
{
    return _id;
}

QString Song::get_title()
{
    QString title("-");
    if (_title != "")
    {
        title = _title;
    }
    return title;
}

QString Song::get_artist()
{
    QString artist("-");
    if (_artist != "")
    {
        artist = _artist;
    }
    return artist;
}

QString Song::get_album()
{
    QString album("-");
    if (_album != "")
    {
        album = _album;
    }
    return album;
}

QString Song::get_image_tag()
{
    return _image_tag;
}

QImage Song::get_icon()
{
    return _icon;
}

void Song::set_icon(QImage a_icon)
{
    _icon = a_icon;
}
