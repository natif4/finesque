#include "itemservice.h"
#include <QUrlQuery>

ItemService::ItemService() : ApiService()
{

}

ItemService::ItemService(const QString &a_token) : ApiService(a_token)
{

}

ItemService::~ItemService()
{
    this->ApiService::~ApiService();
}

QNetworkReply* ItemService::get_song_image(Song *a_song)
{
    set_url(ENDPOINT + "/" + a_song->get_id() + "/Images/Primary");
    QUrlQuery query;
    query.addQueryItem("fillWidth", "80");
    query.addQueryItem("fillHeight", "80");
    query.addQueryItem("tag", a_song->get_image_tag());
    query.addQueryItem("quality", "90");
    set_url_query(query);
    return _network_manager->get(_request);
}

