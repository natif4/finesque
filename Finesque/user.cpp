#include "user.h"

User::User(const QString &a_name,
           const QString &a_id,
           const QString &a_accessToken)
{
    _name = a_name;
    _id = a_id;
    _access_token = a_accessToken;
}

QString User::get_name()
{
    return _name;
}

QString User::get_id()
{
    return _id;
}

QString User::get_access_token()
{
    return _access_token;
}
