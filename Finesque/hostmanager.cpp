#include "hostmanager.h"
#include <QFile>
#include <QDir>

HostManager::HostManager()
{

}

QString HostManager::get_host_url()
{
    QString url = "";
    QFile file(QDir::currentPath() + "/host.txt");
    QTextStream stream(&file);
    if (file.open(QIODevice::ReadOnly))
    {
        QString line = stream.readAll();
        if (line != "")
        {
            url = line;
        }
    }
    file.close();
    return url;
}

void HostManager::create_host_url(const QString &a_scheme,
                                  const QString &a_hostname,
                                  const QString &a_port)
{
    QFile file(QDir::currentPath() + "/host.txt");
    QTextStream stream(&file);
    if (file.open(QIODevice::ReadWrite))
    {
        QString line = stream.readAll();
        if (line == "")
        {
            stream << format_host_url(a_scheme, a_hostname, a_port);
        }
    }
    file.close();
}

QString HostManager::format_host_url(const QString &a_scheme,
                                     const QString &a_hostname,
                                     const QString &a_port)
{
    return a_scheme + "://" + a_hostname + ":" + a_port + "/";
}
