#include "finesqueclient.h"
#include "ui_finesqueclient.h"
#include "song.h"
#include <QList>
#include "jsonserializer.h"
#include <QJsonArray>
#include "authenticationwindow.h"

FinesqueClient::FinesqueClient(User *a_user, QWidget *a_parent) : QMainWindow(a_parent), _ui(new Ui::FinesqueClient)
{
    _ui->setupUi(this);
    if (a_user != nullptr)
    {
        _user = a_user;
        _user_service = new UserService(_user->get_access_token(), _user->get_id());
        _audio_service = new AudioService(_user->get_access_token());
        _api_key_service = new ApiKeyService(_user->get_access_token());
        _item_service = new ItemService(_user->get_access_token());
    }
    _current_song = nullptr;
    _songs = new QList<Song*>;
    _player = new QMediaPlayer;
    _audio_output = new QAudioOutput;
    _is_random = false;
    _random_songs = new QList<Song*>;
    _api_key = "";
    initialize_client_information();
}

FinesqueClient::~FinesqueClient()
{
    delete _ui;
    delete _user;
    delete _current_song;
    delete _user_service;
    delete _audio_service;
    delete _api_key_service;
    delete _item_service;
    delete _songs;
    delete _player;
    delete _random_songs;
}

void FinesqueClient::initialize_client_information()
{
    _ui->tabWidget->setCurrentIndex(SONG_LIST_INDEX);
    _ui->userLabel->setText(_user->get_name() + "'s Library");
    _ui->songListWidget->addItem(QString("Pending..."));

    _ui->iconLabel->setText("No audio selected.");
    _ui->titleLabel->hide();
    _ui->artistLabel->hide();
    _ui->albumLabel->hide();

    _ui->playButton->hide();
    _ui->stopButton->hide();
    _ui->previousButton->hide();
    _ui->nextButton->hide();
    _ui->loopButton->hide();
    _ui->randomButton->hide();
    _ui->durationSlider->hide();
    _ui->volumeSlider->hide();

    _ui->volumeSlider->setValue(BASE_VOLUME);
    _audio_output->setVolume((float)_ui->volumeSlider->value() / MAX_VOLUME);
    _player->setAudioOutput(_audio_output);

    QNetworkReply *reply = _user_service->get_songs();

    QObject::connect(reply, &QNetworkReply::finished, [=]()
    {
        on_song_download_finished(reply);
        QObject::connect(_ui->songListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(on_list_item_clicked(QListWidgetItem*)));
    });

}

void FinesqueClient::initialize_player_information()
{
    _ui->durationSlider->setValue(0);
    QNetworkReply *reply = _api_key_service->get_api_keys();
    QObject::connect(reply, &QNetworkReply::finished, [=]()
    {
        on_api_keys_obtained(reply);
    });

}

void FinesqueClient::on_song_download_finished(QNetworkReply *a_reply)
{
    _ui->songListWidget->clear();
    if(a_reply->error() == QNetworkReply::NoError)
    {
        QJsonObject json = JsonSerializer::deserialize(a_reply);
        QJsonArray json_items = json["Items"].toArray();
        for (int i = 0; i < json_items.size(); i++)
        {
            QJsonObject json_item = json_items[i].toObject();
            QJsonObject json_image_tags = json_item["ImageTags"].toObject();

            Song *song = new Song(json_item["Id"].toString(),
                                  json_item["Name"].toString(),
                                  json_item["AlbumArtist"].toString(),
                                  json_item["Album"].toString(),
                                  json_image_tags["Primary"].toString());

            QNetworkReply *image_reply = _item_service->get_song_image(song);
            QObject::connect(image_reply, &QNetworkReply::finished, [=]()
            {
                on_song_image_download_finished(image_reply, song);
            });
        }
    }
    else
    {
        _ui->songListWidget->addItem(QString("Error."));
        qDebug() << a_reply->errorString();
    }
    a_reply->deleteLater();
}

void FinesqueClient::on_song_image_download_finished(QNetworkReply *a_reply, Song *a_song)
{
    if (a_reply->error() == QNetworkReply::NoError)
    {
        QImage image;
        QPixmap pixmap;
        QListWidgetItem *list_item = new QListWidgetItem;
        image.loadFromData(a_reply->readAll());
        list_item->setIcon(QIcon(pixmap.fromImage(image)));
        a_song->set_icon(image);
        QString song_string = a_song->get_title() + "\n" + a_song->get_artist() + "\n" + a_song->get_album();
        list_item->setText(song_string);
        _ui->songListWidget->addItem(list_item);
        _songs->append(a_song);
    }
    else
    {
        qDebug() << a_reply->errorString();
    }
    a_reply->deleteLater();
}

void FinesqueClient::on_song_audio_download_finished(QNetworkReply *a_reply, Song *a_song)
{
    if (a_reply->error() == QNetworkReply::NoError)
    {
        _ui->titleLabel->setText(a_song->get_title());
        _ui->artistLabel->setText(a_song->get_artist());
        _ui->albumLabel->setText(a_song->get_album());
        _ui->iconLabel->setPixmap(QPixmap().fromImage(a_song->get_icon()));
    }
    else
    {
        qDebug() << a_reply->errorString();
    }
}

void FinesqueClient::on_api_keys_obtained(QNetworkReply *a_reply)
{
    if (a_reply->error() == QNetworkReply::NoError)
    {
        bool api_key_found = find_api_key(a_reply);
        if (!api_key_found && _api_key == "")
        {
            create_api_key();
        }
        else
        {
            load_song(_songs->at(_ui->songListWidget->currentRow()));
        }
    }
    else
    {
        qDebug() << a_reply->errorString();
    }
    a_reply->deleteLater();
}

bool FinesqueClient::find_api_key(QNetworkReply *a_reply)
{
    QJsonObject json = JsonSerializer::deserialize(a_reply);
    QJsonArray json_items = json["Items"].toArray();
    bool api_key_found = false;
    for (auto iterator = json_items.constBegin(); iterator != json_items.constEnd() && !api_key_found; ++iterator)
    {
        QJsonObject json_api_key = iterator->toObject();
        QString api_key_name = json_api_key["AppName"].toString();
        api_key_found = api_key_name == _api_key_service->get_api_key_name();
        if (api_key_found)
        {
            _api_key = json_api_key["AccessToken"].toString();
        }
    }
    return api_key_found;
}

void FinesqueClient::create_api_key()
{
    QNetworkReply *api_key_create_reply = _api_key_service->create_api_key();
    QObject::connect(api_key_create_reply, &QNetworkReply::finished, [=]()
    {
        if (api_key_create_reply->error() == QNetworkReply::NoError)
        {
            QNetworkReply *api_key_reply = _api_key_service->get_api_keys();
            QObject::connect(api_key_reply, &QNetworkReply::finished, [=](){
                if (api_key_reply->error() == QNetworkReply::NoError)
                {
                    bool api_key_found = find_api_key(api_key_reply);
                    if(api_key_found)
                    {
                        load_song(_songs->at(_ui->songListWidget->currentRow()));
                    }
                    else
                    {
                        _ui->iconLabel->setText("Couldn't load information. (API Key invalid)");
                    }
                }
                else
                {
                    qDebug() << "Error getting new api key: " << api_key_reply->errorString();
                }
                api_key_reply->deleteLater();
            });
        }
        else
        {
            qDebug() << "Error creating api key: " << api_key_create_reply->errorString();
        }
        api_key_create_reply->deleteLater();
    });
}

void FinesqueClient::load_song(Song *a_song)
{
    if (a_song != nullptr)
    {
        _current_song = a_song;
        _ui->iconLabel->setText("Loading information...");
        _ui->tabWidget->setCurrentIndex(MEDIA_PLAYER_INDEX);
        QString url = _audio_service->get_audio(a_song->get_id(), _user->get_id(), _api_key);
        if (url != "")
        {
            _ui->iconLabel->setPixmap(QPixmap().fromImage(a_song->get_icon()));
            _ui->titleLabel->show();
            _ui->titleLabel->setText(a_song->get_title());
            _ui->artistLabel->show();
            _ui->artistLabel->setText(a_song->get_artist());
            _ui->albumLabel->show();
            _ui->albumLabel->setText(a_song->get_album());

            _ui->playButton->show();
            _ui->stopButton->show();
            _ui->previousButton->show();
            _ui->nextButton->show();
            _ui->loopButton->show();
            _ui->randomButton->show();
            _ui->durationSlider->show();
            _ui->volumeSlider->show();
            _player->setSource(QUrl(url));
            connect_events_to_player();
        }
    }
}

void FinesqueClient::connect_events_to_player()
{
    connect(_player, &QMediaPlayer::durationChanged, [=]()
    {
        _ui->durationSlider->setMaximum(_player->duration());
        connect(_player, &QMediaPlayer::positionChanged, [=]()
        {
            _ui->durationSlider->setValue(_player->position());
        });
    });

    connect(_player, &QMediaPlayer::playbackStateChanged, [=](){
        switch (_player->playbackState()) {
        case QMediaPlayer::PlayingState:
            _ui->playButton->setText("Pause");
            break;
        case QMediaPlayer::PausedState:
        case QMediaPlayer::StoppedState:
            _ui->playButton->setText("Play");
        default:
            break;
        }
    });

    connect(_player, &QMediaPlayer::mediaStatusChanged, [=](){
        if (_player->mediaStatus() == QMediaPlayer::EndOfMedia &&
            _player->position() == _player->duration())
        {
            if (!_ui->durationSlider->isSliderDown())
            {
                go_to_next_song();
                _player->play();
            }
        }
    });
}

void FinesqueClient::go_to_next_song()
{
    if (_current_song != nullptr)
    {
        int index = _songs->indexOf(_current_song) + 1;
        if (_is_random)
        {
            bool song_was_played = false;
            Song *random_song;
            while(!song_was_played)
            {
                index = rand() % _songs->count();
                random_song = _songs->at(index);
                song_was_played = !_random_songs->contains(random_song);
                if (song_was_played)
                {
                    _random_songs->append(random_song);
                }
            }
            load_song(random_song);
        }
        else
        {
            if (index >= _songs->count())
            {
                index = 0;
            }
            load_song(_songs->at(index));
        }
    }
}

void FinesqueClient::on_logoutButton_clicked()
{
    hide();
    AuthenticationWindow *auth = new AuthenticationWindow();
    auth->show();
    destroy();
}

void FinesqueClient::on_list_item_clicked(QListWidgetItem *a_item)
{
    initialize_player_information();
}

void FinesqueClient::on_playButton_clicked()
{
    if (_player->playbackState() == QMediaPlayer::PausedState ||
        _player->playbackState() == QMediaPlayer::StoppedState)
    {
        _player->play();
    }
    else
    {
        _player->pause();
    }
}

void FinesqueClient::on_stopButton_clicked()
{
    _player->stop();
}

void FinesqueClient::on_volumeSlider_valueChanged(int value)
{
    float volume = (float)value / MAX_VOLUME;
    _audio_output->setVolume(volume);
    _player->setAudioOutput(_audio_output);
}

void FinesqueClient::on_durationSlider_valueChanged(int value)
{
    _player->setPosition(value);
}


void FinesqueClient::on_loopButton_clicked(bool checked)
{
    _player->setLoops(checked ? QMediaPlayer::Infinite : QMediaPlayer::Once);
    _ui->randomButton->setChecked(false);
}


void FinesqueClient::on_nextButton_clicked()
{
    go_to_next_song();
}


void FinesqueClient::on_previousButton_clicked()
{
    if (_ui->durationSlider->value() > 0)
    {
        _ui->durationSlider->setValue(0);
        _player->pause();
    }
    else
    {
        if (_current_song != nullptr)
        {
            QList<Song*> *_song_list = _songs;
            if (_is_random)
            {
                _song_list = _random_songs;
            }
            int index = _song_list->indexOf(_current_song) - 1;
            if (index < 0)
            {
                if (_is_random)
                {
                    index = 0;
                }
                else
                {
                    index = _song_list->count() - 1;
                }
            }
            load_song(_song_list->at(index));
        }
    }
}


void FinesqueClient::on_randomButton_clicked(bool checked)
{
    _is_random = checked;
    _ui->loopButton->setChecked(false);
}

