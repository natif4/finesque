#include "userservice.h"
#include <QDebug>
#include <QObject>
#include "jsonserializer.h"


UserService::UserService() : ApiService()
{
    _user_id = "";
}

UserService::UserService(const QString &a_token, const QString &a_user_id) : ApiService(a_token)
{
    _user_id = a_user_id;
}

UserService::~UserService()
{
    this->ApiService::~ApiService();
}

QNetworkReply* UserService::authenticate_by_name(LoginInformation a_login_information)
{
    set_url(ENDPOINT + QString("/AuthenticateByName"));
    QJsonObject body;
    body["Username"] = a_login_information.get_username();
    body["Pw"] = a_login_information.get_password();
    QByteArray data = JsonSerializer::serialize(body);
    return _network_manager->post(_request, data);
}

QNetworkReply* UserService::get_songs()
{
    set_url(ENDPOINT + "/" + _user_id + "/Items");
    QUrlQuery query;
    query.addQueryItem("SortBy", "Album,SortName");
    query.addQueryItem("SortOrder", "Ascending");
    query.addQueryItem("IncludeItemTypes", "Audio");
    query.addQueryItem("Recursive", "true");
    set_url_query(query);
    return _network_manager->get(_request);
}
