#include "audioservice.h"

AudioService::AudioService() : ApiService()
{

}

AudioService::AudioService(const QString &a_token) : ApiService(a_token)
{

}

AudioService::~AudioService()
{
    this->ApiService::~ApiService();
}

QString AudioService::get_audio(const QString &a_audio_id,
                                const QString &a_user_id,
                                const QString &a_api_key)
{
    set_content_type(ContentType::MP3);
    set_url(ENDPOINT + "/" + a_audio_id + "/universal");
    QUrlQuery query;
    query.addQueryItem("UserId", a_user_id);
    query.addQueryItem("DeviceId", _device_manager->get_device_id());
    query.addQueryItem("api_key", a_api_key);
    query.addQueryItem("MaxStreamingBitrate", "140000000");
    query.addQueryItem("Container", "opus,webm|opus,mp3,aac,m4a|aac,m4b|aac,flac,webma,webm|webma,wav,ogg");
    query.addQueryItem("TranscodingContainer", "ts");
    query.addQueryItem("TranscodingProtocol", "hls");
    query.addQueryItem("AudioCodec", "aac");
    query.addQueryItem("StartTimeTicks", "0");
    set_url_query(query);
    return _request.url().url();
}
