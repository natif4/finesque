#ifndef HOSTMANAGER_H
#define HOSTMANAGER_H

#include <QString>

class HostManager
{
public:
    HostManager();

    QString get_host_url();
    void create_host_url(const QString &a_scheme,
                         const QString &a_hostname,
                         const QString &a_port);

private:
    QString format_host_url(const QString &a_scheme,
                            const QString &a_hostname,
                            const QString &a_port);
};

#endif // HOSTMANAGER_H
