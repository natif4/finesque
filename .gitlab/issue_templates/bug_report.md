## Description

"brief description of the bug"

## Steps to reproduce

"include steps to reproduce the bug"

## Actual behaviour

"how the app is currently behaving"

## Wanted behaviour

"how should the app behave"

## Logs / Screenshots

"any logs / screenshots or usefull proof of the bug"

## Possible fix

"if you think that there's a possible fix, type it here"
