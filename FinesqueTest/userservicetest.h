#ifndef USERSERVICETEST_H
#define USERSERVICETEST_H

#include "servicetest.h"

class UserServiceTest : public ServiceTest
{
public:
    UserServiceTest(TestConfig *a_config);
    void authenticate_by_name();
    void get_songs();
};

#endif // USERSERVICETEST_H
