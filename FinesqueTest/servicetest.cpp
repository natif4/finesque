#include "servicetest.h"

ServiceTest::ServiceTest(TestConfig *a_config, ApiService *a_service)
{
    _config = a_config;
    // The service is casted in every child test class.
    // Using generics with templates causes "undefined reference" to this constructor.
    _service = a_service;
}

ServiceTest::~ServiceTest()
{
    delete _config;
    delete _service;
}

void ServiceTest::wait_for_reply(QNetworkReply* a_reply)
{
    QEventLoop loop;
    connect(a_reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
}



