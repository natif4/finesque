#include "audioservicetest.h"
#include "../Finesque/audioservice.h"

AudioServiceTest::AudioServiceTest(TestConfig *a_config) : ServiceTest(a_config, new AudioService())
{

}

void AudioServiceTest::get_audio()
{
    QString audio_id = _config->get_song()->get_id();
    QString user_id = _config->get_user()->get_id();
    QString api_key = _config->get_api_key("Finesque");
    QString device_id = _config->get_device_id();

    QString url = ((AudioService*)_service)->get_audio(audio_id, user_id, api_key);
    QString audio_url = "Audio/" + audio_id +
                        "/universal?UserId=" + user_id +
                        "&DeviceId=" + device_id +
                        "&api_key=" + api_key;

    QVERIFY(url.toLower().contains(audio_url.toLower()));
}
