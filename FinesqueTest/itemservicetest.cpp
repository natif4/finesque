#include "itemservicetest.h"
#include "../Finesque/itemservice.h"

ItemServiceTest::ItemServiceTest(TestConfig *a_config) : ServiceTest(a_config, new ItemService())
{

}

void ItemServiceTest::get_song_image()
{
    QNetworkReply *reply = ((ItemService*)_service)->get_song_image(_config->get_song());
    wait_for_reply(reply);
    QImage image;
    if (reply->error() == QNetworkReply::NoError)
    {
        image.loadFromData(reply->readAll());
    }
    else
    {
        qDebug() << reply->errorString();
    }
    reply->deleteLater();
    QVERIFY(!image.isNull());
}
