#include "testconfig.h"
#include <QDebug>

TestConfig::TestConfig()
{
    _host_manager = new HostManager();
    _host_manager->create_host_url("", "", ""); // <scheme>://<hostname>:<port>

    // INITIALIZE API KEYS
    _api_keys = new QMap<QString, QString>;
    (*_api_keys)[""] = "";

    // WARNING - DO NOT PUSH CONFIG WITH PASSWORD WRITTEN
    _login = new LoginInformation(USERNAME,
                                  ""); // Username, password

    _user = new User(USERNAME,
                     "",
                     ""); //Username, user id, access token

    _song = new Song("",
                     "",
                     "",
                     "",
                     ""); // Id, title, artist, album, image tag
}

TestConfig::~TestConfig()
{
    delete _user;
    delete _song;
    delete _login;
}

User* TestConfig::get_user()
{
    return _user;
}

Song* TestConfig::get_song()
{
    return _song;
}

QList<QString> TestConfig::get_api_keys()
{
    return _api_keys->values();
}

QString TestConfig::get_api_key(const QString &a_name)
{
    return _api_keys->value(a_name);
}

QString TestConfig::get_device_id()
{
    return DEVICE_ID;
}

QString TestConfig::get_api_key_app_name()
{
    return API_KEY_APP_NAME;
}

LoginInformation TestConfig::get_login_information()
{
    return *_login;
}

size_t TestConfig::get_item_count()
{
    return LIBRARY_ITEM_COUNT;
}


