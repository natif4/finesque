#ifndef ITEMSERVICETEST_H
#define ITEMSERVICETEST_H

#include "servicetest.h"

class ItemServiceTest : public ServiceTest
{
public:
    ItemServiceTest(TestConfig *a_config);
    void get_song_image();
};

#endif // ITEMSERVICETEST_H
