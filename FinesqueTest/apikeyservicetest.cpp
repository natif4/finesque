#include "apikeyservicetest.h"
#include "../Finesque/apikeyservice.h"
#include "../Finesque/jsonserializer.h"
#include <QJsonArray>
#include <QSignalSpy>

ApiKeyServiceTest::ApiKeyServiceTest(TestConfig *a_config) : ServiceTest(a_config, new ApiKeyService(a_config->get_user()->get_access_token()))
{

}

void ApiKeyServiceTest::get_api_keys()
{
    QVERIFY(_config->get_api_keys() == get_current_api_keys());
}

void ApiKeyServiceTest::create_api_key()
{
    QNetworkReply *reply = ((ApiKeyService*)_service)->create_api_key();
    wait_for_reply(reply);
    QVERIFY(reply->error() == QNetworkReply::NoError);
}

void ApiKeyServiceTest::revoke_api_key()
{
    // REVOKES LAST CREATED KEY
    // IF NO KEY WAS CREATED IN THE PREVIOUS METHOD, IT'LL DELETE YOUR LAST KEY
    QList<QString> api_keys = get_current_api_keys();
    QNetworkReply *reply = ((ApiKeyService*)_service)->revoke_api_key(api_keys.last());
    wait_for_reply(reply);
    QVERIFY(reply->error() == QNetworkReply::NoError);
}

QList<QString> ApiKeyServiceTest::get_current_api_keys()
{
    QNetworkReply *reply = ((ApiKeyService*)_service)->get_api_keys();
    wait_for_reply(reply);
    QList<QString> api_keys;
    if (reply->error() == QNetworkReply::NoError)
    {
        QJsonObject json = JsonSerializer::deserialize(reply);
        QJsonArray json_items = json["Items"].toArray();

        for (auto iterator = json_items.constBegin(); iterator != json_items.constEnd(); ++iterator)
        {
            QJsonObject json_api_key = iterator->toObject();
            api_keys.append(json_api_key["AccessToken"].toString());
        }
    }
    else
    {
        qDebug() << reply->errorString();
    }
    reply->deleteLater();
    return api_keys;
}



