#ifndef AUDIOSERVICETEST_H
#define AUDIOSERVICETEST_H

#include "servicetest.h"

class AudioServiceTest : ServiceTest
{

public:
    AudioServiceTest(TestConfig *a_config);
    void get_audio();
};

#endif // AUDIOSERVICETEST_H
