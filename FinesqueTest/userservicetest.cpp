#include "userservicetest.h"
#include "../Finesque/userservice.h"
#include "../Finesque/jsonserializer.h"
#include <QJsonArray>

UserServiceTest::UserServiceTest(TestConfig *a_config) : ServiceTest(a_config, new UserService(a_config->get_user()->get_access_token(),
                                                                                               a_config->get_user()->get_id()))
{

}

void UserServiceTest::authenticate_by_name()
{
    QNetworkReply *reply = ((UserService*)_service)->authenticate_by_name(_config->get_login_information());
    wait_for_reply(reply);
    User *user;
    if (reply->error() == QNetworkReply::NoError)
    {
        QJsonObject json = JsonSerializer::deserialize(reply);
        QJsonObject json_user = json["User"].toObject();

        user = new User(json_user["Name"].toString(),
                        json_user["Id"].toString(),
                        json["AccessToken"].toString());
    }
    else
    {
        qDebug() << reply->errorString();
    }
    reply->deleteLater();
    QVERIFY(_config->get_user()->get_access_token() == user->get_access_token() &&
            _config->get_user()->get_id() == user->get_id() &&
            _config->get_user()->get_name() == user->get_name());
}

void UserServiceTest::get_songs()
{
    QNetworkReply *reply = ((UserService*)_service)->get_songs();
    wait_for_reply(reply);
    QJsonArray json_items;
    if (reply->error() == QNetworkReply::NoError)
    {
        QJsonObject json = JsonSerializer::deserialize(reply);
        json_items = json["Items"].toArray();
    }
    else
    {
        qDebug() << reply->errorString();
    }
    reply->deleteLater();
    QVERIFY(_config->get_item_count() == json_items.count());
}
