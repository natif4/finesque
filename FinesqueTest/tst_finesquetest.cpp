#include <QtTest>
#include <QCoreApplication>

// add necessary includes here
#include "testconfig.h"
#include "audioservicetest.h"
#include "apikeyservicetest.h"
#include "itemservicetest.h"
#include "userservicetest.h"

class FinesqueTest : public QObject
{
    Q_OBJECT

public:
    FinesqueTest();
    ~FinesqueTest();

private slots:
    void audioService_getAudio();
    void apiKeyService_getApiKeys();
    void apiKeyService_createApiKey();
    void apiKeyService_revokeApiKey();
    void itemService_getSongImage();
    void userService_authenticateByName();
    void userService_getSongs();

private:
    TestConfig *_config;
    AudioServiceTest *_audioServiceTest;
    ApiKeyServiceTest *_apiKeyServiceTest;
    ItemServiceTest *_itemServiceTest;
    UserServiceTest *_userServiceTest;
};

FinesqueTest::FinesqueTest()
{
    _config = new TestConfig();
    _audioServiceTest = new AudioServiceTest(_config);
    _apiKeyServiceTest = new ApiKeyServiceTest(_config);
    _itemServiceTest = new ItemServiceTest(_config);
    _userServiceTest = new UserServiceTest(_config);
}

FinesqueTest::~FinesqueTest()
{
    delete _config;
    delete _audioServiceTest;
    delete _apiKeyServiceTest;
    delete _itemServiceTest;
    delete _userServiceTest;
}

void FinesqueTest::audioService_getAudio()
{
    _audioServiceTest->get_audio();
}

void FinesqueTest::apiKeyService_getApiKeys()
{
    _apiKeyServiceTest->get_api_keys();
}

void FinesqueTest::apiKeyService_createApiKey()
{
    _apiKeyServiceTest->create_api_key();
}

void FinesqueTest::apiKeyService_revokeApiKey()
{
    _apiKeyServiceTest->revoke_api_key();
}

void FinesqueTest::itemService_getSongImage()
{
    _itemServiceTest->get_song_image();
}

void FinesqueTest::userService_authenticateByName()
{
    _userServiceTest->authenticate_by_name();
}

void FinesqueTest::userService_getSongs()
{
    _userServiceTest->get_songs();
}

QTEST_MAIN(FinesqueTest)

#include "tst_finesquetest.moc"
