#ifndef APIKEYSERVICETEST_H
#define APIKEYSERVICETEST_H

#include "servicetest.h"

class ApiKeyServiceTest : public ServiceTest
{

public:
    ApiKeyServiceTest(TestConfig *a_config);
    void get_api_keys();
    void create_api_key();
    void revoke_api_key();

private:
    QList<QString> get_current_api_keys();
};

#endif // APIKEYSERVICETEST_H
