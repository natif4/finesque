#ifndef SERVICETEST_H
#define SERVICETEST_H

#include "testconfig.h"
#include "../Finesque/apiservice.h"
#include <QTest>

class ServiceTest : public QObject
{
    Q_OBJECT
public:
    ServiceTest(TestConfig *a_config, ApiService *a_service);
    virtual ~ServiceTest() = 0;

protected:
    TestConfig *_config;
    ApiService *_service;

    void wait_for_reply(QNetworkReply* a_reply);
};

#endif // SERVICETEST_H
