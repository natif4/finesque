#ifndef TESTCONFIG_H
#define TESTCONFIG_H

#include "../Finesque/user.h"
#include "../Finesque/song.h"
#include "../Finesque/hostmanager.h"
#include "../Finesque/logininformation.h"
#include <QMap>

class TestConfig
{
public:
    TestConfig();
    ~TestConfig();

    User* get_user();
    Song* get_song();
    QList<QString> get_api_keys();
    QString get_api_key(const QString &a_name);
    QString get_device_id();
    QString get_api_key_app_name();
    LoginInformation get_login_information();
    size_t get_item_count();

private:
    const QString USERNAME = "";
    const QString DEVICE_ID = "";
    const QString API_KEY_APP_NAME = "";
    const size_t LIBRARY_ITEM_COUNT = 0;

    LoginInformation *_login;
    User *_user;
    Song *_song;
    HostManager *_host_manager;
    QMap<QString, QString> *_api_keys;
};

#endif // TESTCONFIG_H
